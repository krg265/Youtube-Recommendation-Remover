// ==UserScript==
// @name         Youtube Recommendations Remover
// @namespace    http://home.iitk.ac.in/~krgaurav/#!home
// @website      https://github.com/krgaurav94/Youtube-Recommendation-Remover/releases/
// @version      1.1
// @description  Remove annoying recommendation channels
// @author       Kumar Gaurav
// @match        https://www.youtube.com*
// @run-at       document-end
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log('Youtube Recommendations Remover running.');

    var searchText = 'Recommended channel for you';

    function removeParentNode(innerNode){
        while(1){
            if (innerNode.parentNode === null){
                console.log('Failed to fetch the LI element');
                break;
            }
            if(innerNode.parentNode.tagName == 'LI'){
                innerNode.parentNode.style.display = 'none';
                console.log(innerNode.parentNode, 'removed');
                break;
            } else{
                innerNode = innerNode.parentNode;
            }
        }
    }

    function initialCleanup(){
        var spanEls = document.querySelectorAll('.shelf-annotation');
        if (spanEls!==null){
            for (var i=0; i<spanEls.length;i++){
                if (spanEls[i].innerHTML == searchText){
                    removeParentNode(spanEls[i]);
                }
            }
        }
    }

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if(mutation.addedNodes!==null) {
                for (var i=0; i<mutation.addedNodes.length; i++) {
                    if (mutation.addedNodes[i].nodeName != '#text'){
                        var allChildNodes = mutation.addedNodes[i].getElementsByTagName('span');
                        if (allChildNodes !== null){
                            for (var j=0;j<allChildNodes.length;j++){
                                if (allChildNodes[j].innerHTML == searchText){
                                    removeParentNode(allChildNodes[j]);
                                }
                            }
                        }
                    }
                }
            }
        });
    });

    var observerConfig = {
        childList: true,
        subtree: true
    };

    initialCleanup();

    var targetNode = document.querySelector('#content');
    observer.observe(targetNode, observerConfig);
})();